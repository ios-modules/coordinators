// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "Coordinators",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "Coordinators",
            targets: ["Coordinators"]
        ),
        .library(
            name: "MVVMCoordinators",
            targets: ["MVVMCoordinators"]
        )
    ],
    dependencies: [
        .package(url: "https://gitlab.com/k2-ios-modules/mvvm.git", from: "1.0.0")
    ],
    targets: [
        .target(
            name: "Coordinators"
        ),
        .target(
            name: "MVVMCoordinators",
            dependencies: [
                "Coordinators",
                .product(name: "MVVM", package: "MVVM")
            ]
        )
    ]
)
