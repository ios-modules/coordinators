//
//  Created on 22.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension Coordinator {

    /// Тип узла.
    /*
        |           |
        + branch    + branch
        |           |
        |           |
        |           |
        +---trunk---+
        |
        |
        |
        + branch
        |
        |
        |
        + branch
        |
        |
        | navigation stack
     */
    public enum NodeType {
        case branch
        case trunk
    }
}
