//
//  Created on 22.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol Coordinatable {
    func openCoordinator(_ coordinator: Coordinator)
}
