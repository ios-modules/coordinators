//
//  Created on 14.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import UIKit
import Combine

public typealias CloseControllerSubject = PassthroughSubject<Void, Never>

public protocol MovableController where Self: UIViewController {

    /// Издатель о фактическом случае закрытия контроллера.
    var closeControllerSubject: CloseControllerSubject { get }
}

public extension MovableController {

    /// Отправить подписчику сигнал о фактическом закрытии контроллера.
    /// `Important`: Необходимо вызывать в методе `viewDidDisappear`.
    func sendCloseIfNeeded() {
        if self.isInHierarchy == false || self.navigationController?.isInHierarchy == false {
           self.closeControllerSubject.send()
        }
    }
}
