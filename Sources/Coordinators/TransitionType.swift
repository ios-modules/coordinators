//
//  Created on 14.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import UIKit

public enum TransitionType {
    case pushing(Pushing = .init())
    case presenting(Presenting = .init())
}

// MARK: - Pushing

extension TransitionType {
    public struct Pushing {
        public let animated: Bool

        public init(animated: Bool = true) {
            self.animated = animated
        }
    }
}

// MARK: - Presenting

extension TransitionType {
    public struct Presenting {
        public let modalPresentationStyle: UIModalPresentationStyle
        public let modalTransitionStyle: UIModalTransitionStyle
        public let transitioningDelegate: UIViewControllerTransitioningDelegate?
        public let withNavigation: Bool
        public let animated: Bool

        public init(
            modalPresentationStyle: UIModalPresentationStyle = .fullScreen,
            modalTransitionStyle: UIModalTransitionStyle = .coverVertical,
            transitioningDelegate: UIViewControllerTransitioningDelegate? = nil,
            withNavigation: Bool = false,
            animated: Bool = true
        ) {
            self.modalPresentationStyle = modalPresentationStyle
            self.modalTransitionStyle = modalTransitionStyle
            self.transitioningDelegate = transitioningDelegate
            self.withNavigation = withNavigation
            self.animated = animated
        }
    }
}
