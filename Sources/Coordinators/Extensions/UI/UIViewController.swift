//
//  Created on 14.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import UIKit

public extension UIViewController {
    var isInHierarchy: Bool {
        self.isBeingPresented ||
        self.presentingViewController != nil ||
        self.presentedViewController != nil ||
        self.parent != nil ||
        self.view.window != nil ||
        self.navigationController != nil ||
        self.tabBarController != nil ||
        self.splitViewController != nil
    }

    func performAlongsideTransition(
        animation: (() -> Void)? = nil,
        completion: (() -> Void)? = nil
    ) {
        guard let transitionCoordinator else {
            animation?()
            completion?()
            return
        }
        transitionCoordinator.animate(
            alongsideTransition: { _ in animation?() },
            completion: { _ in completion?() }
        )
    }
}

