//
//  Created on 14.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import UIKit

public extension UINavigationController {
    func pushViewController(
        _ vc: UIViewController,
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        self.pushViewController(vc, animated: animated)
        self.performAlongsideTransition(completion: completion)
    }

    func popViewController(
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        self.popViewController(animated: animated)
        self.performAlongsideTransition(completion: completion)
    }

    func popToViewController(
        _ vc: UIViewController,
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        self.popToViewController(vc, animated: animated)
        self.performAlongsideTransition(completion: completion)
    }

    func popToRootViewController(
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        self.popToRootViewController(animated: animated)
        self.performAlongsideTransition(completion: completion)
    }
}
