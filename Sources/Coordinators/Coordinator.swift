//
//  Created on 14.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import UIKit
import Combine

open class Coordinator: Coordinatable {

    // MARK: - variables

    public var cancellables: Set<AnyCancellable> = []

    // MARK: - properties

    public private(set) var customPresentingTransitioningDelegate: UIViewControllerTransitioningDelegate?

    /// Родительский координатор.
    public private(set) weak var parentCoordinator: Coordinator?

    /// Стек дочерних координаторов.
    public private(set) var launchedCoordinators: [Coordinator] = []

    /// Блок закрытия координатора.
    /// Выполняется асинхронно на главном потоке.
    public var closeCompletion: (@MainActor () async -> Void)?

    /// Тип узла координатора.
    public var nodeType: NodeType = .branch

    /// Корневой контроллер - отображаемый экран.
    public let rootVC: MovableController

    /// Тип перехода контроллера.
    public let transitionType: TransitionType

    // MARK: - init

    public init(
        rootVC: MovableController,
        transitionType: TransitionType = .pushing()
    ) {
        self.rootVC = rootVC
        self.transitionType = transitionType
        self.setBinding()
    }

    deinit {
        self.customPresentingTransitioningDelegate = nil
    }

    // MARK: - binding

    open func setBinding() {
        self.rootVC.closeControllerSubject
            .sink { [weak self] in
                guard let self else { return }

                Task { @MainActor in
                    await self.closeCompletion?()
                }

                self.parentCoordinator?.launchedCoordinators.removeAll {
                    $0.rootVC === self.rootVC
                }
            }
            .store(in: &self.cancellables)
    }

    // MARK: - launth methods

    /// Запустить текущий координатор на родительском.
    /// Переопределить, если необходимо, подготовить запуск с некоторой логикой.
    /// - Parameter parentCoordinator: Родительский координатор.
    open func launchCoordinator(on parentCoordinator: Coordinator) {
        let parentVC = parentCoordinator.rootVC
        let targetVC = self.prepareRootVC()
        switch self.transitionType {
        case let .pushing(config):
            parentVC.navigationController?.pushViewController(
                targetVC,
                animated: config.animated
            )

        case let .presenting(config):
            parentVC.present(
                targetVC,
                animated: config.animated,
                completion: nil
            )
        }
        parentCoordinator.appendLaunchedCoordinator(self)
        self.parentCoordinator = parentCoordinator
    }

    /// Открыть координатор на текущем координаторе.
    /// - Parameter coordinator: Координатор для открытия.
    open func openCoordinator(_ coordinator: Coordinator) {
        guard coordinator !== self else { return }
        coordinator.launchCoordinator(on: self)
    }

    // MARK: - launched coordinators

    /// Добавить координатор в стек дочерних координаторов.
    /// - Parameter coordinator: Целевой координатор.
    public final func appendLaunchedCoordinator(_ coordinator: Coordinator) {
        self.launchedCoordinators.append(coordinator)
    }

    /// Очистить стек дочекних координаторов.
    public final func clearLaunchedCoordinators() {
        self.launchedCoordinators.removeAll()
    }

    // MARK: - prepare

    /// Подготовить корневой контроллер.
    /// Для случая презентации контроллера, корневой контроллер может быть навигационным контроллером.
    /// - Returns: Корневой контроллер.
    public final func prepareRootVC() -> UIViewController {
        switch self.transitionType {
        case .pushing:
            return self.rootVC

        case let .presenting(config):
            let vc = config.withNavigation
            ? self.makePresentedNavigationVC()
            : self.rootVC
            self.preparePresentedVC(vc, config: config)
            return vc
        }
    }

    /// Подготовить презентуемый контроллер.
    /// - Parameters:
    ///   - vc: Целевой контроллер.
    ///   - config: Конфигурация презентации контроллера.
    open func preparePresentedVC(_ vc: UIViewController, config: TransitionType.Presenting) {
        vc.modalPresentationStyle = config.modalPresentationStyle
        vc.modalTransitionStyle = config.modalTransitionStyle

        let transitioningDelegate = config.transitioningDelegate
        self.customPresentingTransitioningDelegate = transitioningDelegate
        vc.transitioningDelegate = transitioningDelegate
    }

    // MARK: - navigation

    /// Создать навигационный контроллер для презентации.
    /// - Returns: Наивгационный контроллер.
    open func makePresentedNavigationVC() -> UINavigationController {
        UINavigationController(rootViewController: self.rootVC)
    }
    
    /// Навигировать по указанному маршруту.
    /// Переопределить для описания переходов по маршрутам.
    /// - Parameter route: Маршрут навигации.
    open func navigate(to route: NavigationRoutable) {

    }

    // MARK: - search for a coordinator

    /// Получить самыйх верхний координатор (видимый экран) в стеке дочерних коорданторов (снизу вверх).
    /// - Returns: Целевой координатор.
    public final func topCoordinator() -> Coordinator? {
        self.launchedCoordinators.isEmpty
        ? self
        : self.launchedCoordinators.last?.topCoordinator()
    }

    /// Получить координатор указанного типа узла в иерархии координаторов (сверху вниз).
    /// - Parameter node: Тип узла.
    /// - Returns: Координатор целевого узла (если не находит `.truck`, то возвращает кореневой).
    public func nodeCoordinator(_ node: NodeType) -> Coordinator {
        guard let parentCoordinator else { return self }

        return parentCoordinator.nodeType == node
        ? parentCoordinator
        : parentCoordinator.nodeCoordinator(node)
    }

    /// Получить родительский координатор указанного типа в иерархии координаторов (сверху вниз).
    /// - Parameter type: Целевой тип координатора.
    /// - Returns: Целевой родительский координатор.
    public final func deepParentCoordinator<T: Coordinator>(_ type: T.Type) -> T? {
        guard let parentCoordinator else { return nil }

        if let coordinator = parentCoordinator as? T {
            return coordinator
        } else {
            return parentCoordinator.deepParentCoordinator(T.self)
        }
    }

    // MARK: - closing coordinator

    /// Закрыть координатор.
    /// Под капотом происходит `popViewController` или `dismiss`.
    /// - Parameters:
    ///   - animated: Флаг анимации закрытия.
    ///   - completion: Блок, срабатываемый после закрытия контроллера.
    public final func closeCoordinator(
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        switch self.transitionType {
        case .pushing:
            if let completion {
                self.nc?.popViewController(animated: animated, completion: completion)
            } else {
                self.nc?.popViewController(animated: animated)
            }

        case .presenting:
            if let completion {
                self.rootVC.dismiss(animated: animated, completion: completion)
            } else {
                self.rootVC.dismiss(animated: animated)
            }
        }
    }

    /// Закрыть координатор и открыть следующий.
    /// Под капотом происходит `popViewController` или `dismiss`, а после `openCoordinator`.
    /// - Parameters:
    ///   - coordinator: Координатор для открытия.
    ///   - animated: Флаг анимации закрытия.
    ///   - completion: Блок, срабатываемый после закрытия контроллера.
    open func closeAndOpenCoordinator(
        _ coordinator: Coordinator,
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        let parentCoordinator = self.parentCoordinator
        self.closeCoordinator(animated: animated, completion: {
            parentCoordinator?.openCoordinator(coordinator)
        })
    }
    
    /// Закрыть координатор и выполнить навигирование на родительском координаторе.
    /// - Parameter route: Маршрут наивгации.
    public final func closeAndRoutingFromParent(_ route: NavigationRoutable) {
        self.closeCoordinator { [weak self] in
            self?.parentCoordinator?.navigate(to: route)
        }
    }

    /// Закрыть координатор до корневого координатора.
    /// Под капотом происходит `popToRootViewController` или `dismiss`.
    /// - Parameters:
    ///   - animated: Флаг анимации закрытия.
    ///   - completion: Блок, срабатываемый после закрытия контроллера.
    public final func closeToRootCoordinator(
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        switch self.transitionType {
        case .pushing:
            if let completion {
                self.nc?.popToRootViewController(animated: animated, completion: completion)
            } else {
                self.nc?.popToRootViewController(animated: animated)
            }

        case .presenting:
            guard let parentCoordinator else {
                self.rootVC.presentingViewController?.dismiss(animated: animated, completion: completion)
                return
            }

            switch parentCoordinator.transitionType {
            case .pushing:
                self.rootVC.presentingViewController?.dismiss(animated: animated, completion: completion)

            case .presenting:
                parentCoordinator.closeToRootCoordinator(animated: animated, completion: completion)
            }
        }
    }

    /// Закрыть координатор до текущего координатора.
    /// Под капотом происходит `popToViewController` или `dismiss`.
    /// - Parameters:
    ///   - animated: Флаг анимации закрытия.
    ///   - completion: Блок, срабатываемый после закрытия контроллера.
    public final func closeToSelfCoordinator(
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        let vc = self.rootVC
        switch self.transitionType {
        case .pushing:
            if let completion {
                self.nc?.popToViewController(vc, animated: animated, completion: completion)
            } else {
                self.nc?.popToViewController(vc, animated: animated)
            }

        case .presenting:
            if let navigationController = self.nc {
                if let completion {
                    navigationController.popToViewController(vc, animated: animated, completion: completion)
                } else {
                    navigationController.popToViewController(vc, animated: animated)
                }
            } else {
                self.rootVC.presentedViewController?.dismiss(animated: animated, completion: completion)
            }
        }
    }

    /// Закрыть координатор для указаного узла.
    /// - Parameters:
    ///   - node: Целевой узел.
    ///   - animated: Флаг анимации закрытия.
    ///   - completion: Блок, срабатываемый после закрытия контроллера.
    public final func closeCoordinator(
        to node: NodeType,
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        switch node {
        case .branch:
            self.closeCoordinator(animated: animated, completion: completion)

        case .trunk:
            let trunkCoordinator = self.nodeCoordinator(.trunk)
            trunkCoordinator.closeToSelfCoordinator(animated: animated, completion: completion)
        }
    }
}

// MARK: - priavte

private extension Coordinator {
    var nc: UINavigationController? {
        self.rootVC.navigationController
    }
}
