//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import Coordinators
import Foundation
import MVVM

public protocol MVVMControllable: MovableController,
                                  MVVMEntity
where ViewModel: ControllerViewModelable { }
