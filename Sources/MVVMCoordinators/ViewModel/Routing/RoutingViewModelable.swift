//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import Combine
import Coordinators
import Foundation
import MVVM

public protocol RoutingViewModelable: MVVM.ViewModelable {
    typealias RoutingSubject = PassthroughSubject<NavigationRoutable, Never>

    var routing: RoutingSubject { get }
}

public extension RoutingViewModelable {
    func subscribeRouting(_ target: RoutingSubject) {
        self.routing
            .subscribe(target)
            .store(in: &self.cancellables)
    }
}
