//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import Foundation
import MVVM

open class RoutingViewModel: MVVM.ViewModel,
                             RoutingViewModelable {

    // MARK: - properties

    public private(set) var routing: RoutingSubject = .init()

}
