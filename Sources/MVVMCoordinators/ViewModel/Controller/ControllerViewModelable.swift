//
//  Created on 15.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import Combine
import Foundation
import MVVM

public protocol ControllerViewModelable: MVVM.ControllerViewModelable, RoutingViewModelable {

}
