//
//  Created on 15.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation
import MVVM

open class ControllerViewModel: MVVM.ControllerViewModel,
                                ControllerViewModelable {

    // MARK: - properties

    public private(set) var routing: RoutingSubject = .init()

}
