//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Coordinators
import Foundation
import MVVM

open class MVVMCoordinator<
    VC: MVVMControllable
>: Coordinator {

    public typealias ViewModel = VC.ViewModel

    // MARK: - properties

    /// controller with view model
    private let vc: VC

    public var viewModel: ViewModel { self.vc.viewModel }

    // MARK: - init

    public init(
        vm: ViewModel,
        transitionType: TransitionType = .pushing()
    ) {
        self.vc = .init(vm: vm)
        super.init(rootVC: self.vc, transitionType: transitionType)
    }

    // MARK: - binding

    open override func setBinding() {
        super.setBinding()

        self.viewModel.routing
            .sink { [weak self] in self?.navigate(to: $0) }
            .store(in: &self.cancellables)
    }
}
